import java.util.List;

public interface IApp {

  int getNumberOfCities();
  List < String > getNameOfCities(int numberOfCities);
  int menuAddRoad();
  void addRoad(Agglomeration A);

  int menuAddSchool();

  void quit();

  void addSchool(Agglomeration A);
  void deleteSchool(Agglomeration A);
  void printSchools(Agglomeration A);

}