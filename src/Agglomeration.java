import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Agglomeration {
	// L'ID d'une ville correspond � son indice de ligne dans la matrice d'adjacence
	
	
  // Faire la correspondence entre un nom de ville et une ville via une Map
  private Map < String,
  City > cities;
  // Faire la correspondence entre l'ID d'une ville et son nom via une Map
  private Map < Integer,
  String > indexToCityName;
  // Matrice d'adjacence
  private boolean adjacence[][];
  //Nombre(s) de ville(s)
  private int numberOfCities;

  /*
     * Contructeur de Agglomeration initialisant l'ensemble des attributs
     * @param : cityNames une liste de nom de ville
     */
  Agglomeration(List < String > cityNames) {
    numberOfCities = cityNames.size();
    adjacence = new boolean[numberOfCities][numberOfCities];

    cities = new HashMap < String,
    City > ();
    indexToCityName = new HashMap < Integer,
    String > ();
    initHashMaps(cityNames);

  }

  /*
     * Permet l'initialisation des deux HashMaps cities et indexToCityName
     * @param cityNames la liste de nom de ville
     */
  private void initHashMaps(List < String > cityNames) {
    int index = 0;
    for (String c: cityNames) {
      indexToCityName.put(index++, c);
      cities.put(c, new City(c));
    }

  }
  /*
   * Permet de r�cup�rer la liste compos�e des villes de l'agglom�ration
   */
  
  public Collection<City> getCities(){
	 return cities.values();
  }

  /*
     * Permet de r�cup�rer une ville via son nom
     * @param cityName nom de la ville
     * @return la ville si elle existe , null sinon
     */
  public City getCityByName(String cityName) {
    return cities.get(cityName);
  }

  /*
     * Permet de r�cup�rer une ville via son id
     * @param cityID id de la ville
     * @return la ville si elle existe, null sinon
     */
  public City getCityByID(Integer cityID) {
    return cities.get(indexToCityName.get(cityID));
  }

  /*
     * Permet de v�rifier qu'une ville appartient � l'agglom�ration
     * @param city Nom de la ville
     * @return true si la ville appartient � l'agglom�ration, false sinon
     */
  public boolean contains(String cityName) {
    return (cities.get(cityName) != null);
  }

  /*
     * Permet de r�cup�rer la liste des villes reli� � une ville pass� en param�tre
     * @param city Ville pour laquelle on souhaite r�cup�rer ses villes voisines
     * @return Une liste de ville contenant les villes voisines
     */

  public List < City > getNeighbor(City city) {
    List < City > neighbor = new ArrayList < City > ();
    int indexCity = city.getCityID();
    int i = 0;
    for (i = 0; i < numberOfCities; i++) {
      if (adjacence[indexCity][i]) {
        neighbor.add(getCityByID(i));
      }
    }
    return neighbor;
  }

  /*
    * Permet de r�cup�rer le nombre d'�cole auquel est reli� une ville
    * Elle prend en compte �galement l'�cole de la ville pass� en param�tre
    * @param city Ville dont on souhaite conna�tre le nombre d'�cole auquel elle est connect�
    * @eturn le nombre d'�cole auquel elle est effectivement connect�
    */
  public int connectedTo(City city) {
    int nbOfSchool;
    List < City > neighbor = getNeighbor(city);
    nbOfSchool = city.getHasSchool() ? 1 : 0;

    for (City c: neighbor) {
      if (c.getHasSchool()) {
        nbOfSchool++;
      }
    }
    return nbOfSchool;
  }

  /*
     * Permet de r�cup�rer une liste de ville dependante de la ville pass� en param�tre
     * @param city Ville o� l'on souhaite d�truire l'�cole
     * @return liste de villes indiquant les villes qui se retrouverait sans �cole si l'�cole de la ville pass� en param�tre venait � �tre d�truite
     */
  public List < City > Accessibility(City city) {
    List < City > withoutSchool = new ArrayList < City > ();
    List < City > neighbor = getNeighbor(city);

    // Si la ville n'est reli� � aucune autre ville  ou bien que la ville n'est reli� � aucune autre �cole que la sienne
    // Alors elle d�pend d'elle m�me , on l'ajoute donc dans la liste
    if (neighbor.size() == 0 || connectedTo(city) == 1) withoutSchool.add(city);

    // V�rifie que chaque ville  voisine n'est oas d�pendant de la ville pass� en param�tre
    // Si une ville l'est alors on l'ajoute � la liste
    for (City neighboringCity: neighbor) {
      if (neighboringCity.getHasSchool() == false) {
        if (connectedTo(neighboringCity) == 1) {
          withoutSchool.add(neighboringCity);
        }
      }
    }

    return withoutSchool;
  }

  /*
     * Permet de savoir si l'on peut supprimer une �cole dans une ville pass� en param�tre sans rompre la contrainte d'accessibilit�
     * la contrainte d'accessibilit�: chaque ville est reli� � une �cole via une route ou en poss�de une.
     * @param city Ville dans laquel on souhaite supprimer l'�cole
     * @return true si la ville peut �tre supprim�, false sinon
     */
  public boolean canIdeleteSchool(City city) {
    return Accessibility(city).isEmpty();
  }

  /*
     * Permet de cr�er une route entre deux villes.
     * @param cityA nom de la premiere ville que l'on souhaite relier
     * @param cityBville nom de la seconde ville l'on souhaite relier
     * @return true si la route a �t� cr��e , false si une route existe d�j�
     */
  public boolean createRoad(String cityA, String cityB) {
    int indexCityA = getCityByName(cityA).getCityID();
    int indexCityB = getCityByName(cityB).getCityID();
    if(adjacence[indexCityA][indexCityB] == true) 
    	return false;
    else {
    	adjacence[indexCityA][indexCityB] = true;
        adjacence[indexCityB][indexCityA] = true;
        return true;
    }
    
    
  }

  /*
     * Permet d'ajouter une �cole � une ville
     * @param cityName nom de la ville o� l'on souahite ajouter une �cole
     * @return renvoie true si l'�cole a �t� construite, false si la ville poss�de deja une ecole
     */
  public boolean addSchool(String cityName) {
    City city = getCityByName(cityName);
    
    return city.buildSchool();
   
  }

  /*
     * Permet de supprimer une �cole dans une ville
     * @param nom de la ville o� l'on souhaite supprimer l'�cole
     * @return renvoie true si l'�cole a bien �t� supprim�, false si la ville ne poss�de pas d'�cole
     */
  public boolean deleteSchool(String cityName) {
    City city = getCityByName(cityName);
    
    return city.deleteSchool();
  }

}