import java.util.List;

public class Main {

  public static void main(String args[]) {
    IApp app = new App();
    int nbCities;
    List < String > cityNames;
    Agglomeration A;
    int choix = 0;

    nbCities = app.getNumberOfCities();
    cityNames = app.getNameOfCities(nbCities);

    A = new Agglomeration(cityNames);
    do {
      choix = app.menuAddRoad();
      if (choix == 1) app.addRoad(A);
    } while ( choix != 2 );

    do {
      choix = app.menuAddSchool();
      if (choix == 1) app.addSchool(A);
      if (choix == 2) app.deleteSchool(A);
      if (choix != 3) app.printSchools(A);

    } while ( choix != 3 );

    app.quit();

  }

}