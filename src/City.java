public class City {
  private static int nCity = 0; // Compteur de nombre d'instance de ville
  private String cityName; // Nom de la ville
  private boolean hasSchool; //indique si la ville a une �cole
  private int cityID = 0; // Numero/ID de la ville

  /*
	 * Constructeur de City, par d�fault, une ville a une �cole
	 * @param cityName nom de la ville � cr�er
	 */
  City(String cityName) {
    this.cityName = cityName;
    hasSchool = true;
    cityID = nCity++;
  }

  /*
	 * Permet de r�cup�rer le nom de la ville
	 * @return nom de la ville
	 */
  public String getCityName() {
    return cityName;
  }
  /*
	 * Permet de savoir si la ville poss�de une �cole
	 * @return true si la ville a une �cole, false sinon
	 */
  public boolean getHasSchool() {
    return hasSchool;
  }
  /*
	 * Permet de r�cup�rer l'ID de la ville
	 * @return identifiant num�rique de la ville
	 */
  public int getCityID() {
    return cityID;
  }

  /*
	 * Permet de construire une �cole dans la ville
	 * @return true si l'�cole a �t� construite, false si la ville poss�de d�j� une �cole
	 */

  public boolean buildSchool() {
    if (hasSchool == false) {
      hasSchool = true;
      return true;
    }

    return false;
  }

  /*
	 * Permet de d�truire une �cole dans la ville
	 * @return true si l'�cole a �t� d�truite,false si la ville n'a pas d'�cole
	 */
  public boolean deleteSchool() {
    if (hasSchool) {
      hasSchool = false;
      return true;
    }
    return false;

  }

}