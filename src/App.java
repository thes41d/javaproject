import java.util.ArrayList;
import java.util.Collection;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class App implements IApp {
  private Scanner sc = new Scanner(System. in );
  @Override
  public int getNumberOfCities() {

    int nbCities = 0;

    do {
      try {
        System.out.println("Nombre de ville ? : ");
        nbCities = sc.nextInt();

      } catch(InputMismatchException e) {
        System.err.println("La donn�e saisie est incorrecte");
        nbCities = -1;
      } finally {
        sc.nextLine(); // Vider le buffer
      }
    } while ( nbCities < 1 );

    return nbCities;
  }
  @Override
  public List < String > getNameOfCities(int numberOfCities) {

    List < String > cityNames = new ArrayList < String > ();
    StringBuilder sb = new StringBuilder();
    int i = 1;

    for (i = 1; i <= numberOfCities; i++) {
      System.out.println("Nom de la ville " + i + " : ");

      sb.append(sc.next());
      if (cityNames.contains(sb.toString())) {
        System.err.println("Vous avez deja une ville de ce nom");
        i--;
      } else {
        cityNames.add(sb.toString());
      }
      sb.delete(0, sb.length());
    }
    return cityNames;
  }

  @Override
  public int menuAddRoad() {
    int choix = 0;
    do {
      try {
        System.out.println("1) ajouter une route");
        System.out.println("2) fin.");
        choix = sc.nextInt();
      } catch(InputMismatchException e) {
        System.err.println("La donn�e saisie est incorrecte");
        choix = -1;
      } finally {
        sc.nextLine();
      }
    } while (( choix != 2 ) && (choix != 1));

    return choix;
  }

  @Override
  public int menuAddSchool() {
    int choix = 0;
    do {
      try {
        System.out.println("1) ajouter une �cole");
        System.out.println("2) retirer une �cole");
        System.out.println("3) fin");
        choix = sc.nextInt();
      } catch(InputMismatchException e) {
        System.err.println("La donn�e saisie est incorrecte");
        choix = -1;
      } finally {
        sc.nextLine();
      }
    } while (( choix != 2 ) && (choix != 1) && (choix != 3));

    return choix;

  }

  @Override
  public void quit() {
    System.out.println("Fin du programme ! ");
    sc.close();
  }

  @Override
  public void addRoad(Agglomeration A) {
    String cityA = new String();
    String cityB = new String();
    boolean validName = true;

    do {
      System.out.println("Ville 1 : ");
      cityA = sc.next();
      validName = A.contains(cityA);
      if(!validName)
  		System.err.println("La ville saisie n'existe pas !");
    } while (! validName);

    do {
      System.out.println("Ville 2 : ");
      cityB = sc.next();
      validName = A.contains(cityB);
      if(!validName)
  		System.err.println("La ville saisie n'existe pas !");

    } while (! validName);
    
    if(A.createRoad(cityA, cityB)) 
    	System.out.println("La route entre " + cityA + " et " + cityB + " a bien �t� construite");
    else
    	System.out.println("Une route existe d�j� entre " + cityA + " et " + cityB + " !");
  }

  @Override
  public void addSchool(Agglomeration A) {
    String cityName = new String();
    boolean validName = false;

    do {
      System.out.println("Ville dans laquelle ajouter une �cole : ");
      cityName = sc.next();
      validName = A.contains(cityName);
      if(!validName)
    		System.err.println("La ville saisie n'existe pas !");
    } while (! validName);

    if(A.addSchool(cityName))
    	System.out.println("L'�cole dans la ville de " + cityName + " a bien �t� construite");
    else 
    	System.out.println(cityName + " poss�de d�j� une �cole !");
  }

  @Override
  public void deleteSchool(Agglomeration A) {
    String cityName = new String();
    City city;
    List<City> withoutSchool;
    boolean validName;

    do {
      System.out.println("Ville dans laquelle enlever une �cole : ");
      cityName = sc.next();
      validName = A.contains(cityName);
      if(!validName)
  		System.err.println("La ville saisie n'existe pas !");

    } while (! validName);
    
    city = A.getCityByName(cityName);
    withoutSchool = A.Accessibility(city);
    
    if(A.canIdeleteSchool(city)){
    	if(A.deleteSchool(cityName.toString()))
        	System.out.println("L'�cole dans la ville de " + cityName.toString() + " a bien �t� d�truite");
        else
        	System.out.println(cityName.toString() + " n'a pas d'�cole !");
    }
    else {
    	System.out.println("Vous ne pouvez pas supprimer l'ecole dans la ville de " + cityName);
        System.out.print("Ville(s) qui se retrouverai(ent) sans ecole : ");
        for (City c: withoutSchool)
            System.out.print(c.getCityName() + " ");
        System.out.println();
    }
    
  }

  @Override

  public void printSchools(Agglomeration A) {
    Collection < City > cities = A.getCities();
    if(cities.size() > 1)
    	System.out.println("Les �coles se trouvent actuellement �  : ");
    else
    	System.out.println("L'�cole se trouve actuellement �  : ");
   
    for (City city: cities) {
      if (city.getHasSchool()) {
        System.out.print(city.getCityName() + " ");
      }
    }
    System.out.println();
  }

}